<?php

if(isset($_POST['email'])) {
	
		$email_to = "sales@willowelm.com";
		$email_subject = "Contact request willowelm.com ".date(DATE_RFC2822);
 
    function onError($error) {
    	echo "There are some error(s) in form you submitted. ";
    	echo "These errors appear below.<br /><br />";
    	echo $error."<br /><br />";
    	echo "Please go back and fix these errors.<br /><br />";
    	die();
    }
    
    // validation expected data exists
    if(!isset($_POST['name']) || !isset($_POST['email']) || !isset($_POST['message'])) {
    	onError('Please fill all the fields in the form.');
    }
    
    $name = $_POST['name'];
    $subject = isset($_POST['subject']) ? $_POST['subject'] : false;
    $email_from = $_POST['email'];
    $phone = isset($_POST['phone']) ? $_POST['phone'] : false;
    $state = isset($_POST['state']) ? $_POST['state'] : false;
    $message = $_POST['message'];
 
    $error_message = "";
    $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
    
    if(!preg_match($email_exp, $email_from)) {
    	$error_message .= 'The Email Address you entered does not appear to be valid.<br />';
    }
    
    $string_exp = "/^[A-Za-z .'-]+$/";
    
    if(!preg_match($string_exp, $name)) {
    	$error_message .= 'The Name you entered does not appear to be valid. <br />';
    }
    
    if(!preg_match($string_exp, $subject)) {
    	$error_message .= 'The subject you entered does not appear to be valid. <br />';
    }
    
    if(strlen($message) < 2) {
    	$error_message .= 'The message you entered do not appear to be valid. <br />';
    }
    
    if(strlen($error_message) > 0) {
    	onError($error_message);
    }
 
    $email_msg = "Form details: \n\n";
    
    function clean_string($string) {
      $bad = array("content-type","bcc:","to:","cc:","href");
      return str_replace($bad, '', $string);
    }
    
    $email_msg .= "Name: ".clean_string($name)."\n";
    if ($subject){
    	$email_msg .= "Subject: ".clean_string($subject)."\n";
    }
    $email_msg .= "Email: ".clean_string($email_from)."\n";
    if ($phone){
    	$email_msg .= "Phone: ".clean_string($phone)."\n";
    }
    if ($state){
    	$email_msg .= "Landscaping: ".clean_string($state)."\n";
    }
    $email_msg .= "Message: ".clean_string($message)."\n";
    
    // create email headers
    $headers = 'From: '.$email_from."\r\n".'Reply-To: '.$email_from."\r\n" .'X-Mailer: PHP/'.phpversion();
    
    mail($email_to, $email_subject, $email_msg, $headers);  

?>
 
Thank you for contacting us. We will be in touch with you very soon.
 
<?php
 
}
?>
